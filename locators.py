from selenium.webdriver.common.by import By


# _TF - text field
# _IV  - parameter input values
# _CB - check box
# _LB - label
# _BT - buttonProductPage
# _DD - dropdown
# _RB - radio button
# for maintainability we can seperate web objects by page name


class LoginPageLocators(object):
    USERNAME_TF = (By.XPATH, "//input[@id='j_username']")
    PASSWORD_TF = (By.XPATH, "//input[@id='j_password']")
    SUBMIT_BT = (By.XPATH, "//button[@id='submit_login']")
    ALERT_TM = (By.XPATH, "//div[@class='alert alert-danger fade in fade-in-alert']")


class TenderAdmin(object):
    # Icons
    LOGOUT_BT = (By.XPATH, "//div[@id='logout']")
    PORTAL_BT = (By.XPATH, "//div[@id='portal']")
    # Navigation menu
    CRM_LB = (By.XPATH, "//*[@href='/admin/crm/index']")
    CMS_LB = (By.XPATH, "//*[@href='/admin/cmsPage/index']")
    PROFILE_ADMIN_LB = (By.XPATH, "//*[@href='/admin/searchProfile/index']")
    TENDER_MANAGEMENT_LB = (By.XPATH, "//*[@href='/admin/apolloStatistic/index/dataSource']")
    ADMINISTRATION_LB = (By.XPATH, "//*[@href='/admin/manager/list']")
    ALL_MENU_OPTIONS = (By.XPATH, "//div[@class='navbar navbar-default']//li")
    # Administration -> Portal Data
    EDIT_MANAGERS_LB = (By.XPATH, "//*[@href='/admin/manager/index']")
    USER_NAME_LB = (By.XPATH, "//div[@class='login-info']//span//a")


class PortalData(object):
    VIEW_TITLE_BAR = (By.XPATH, "//div[@class='panel-heading bg-color-darken txt-color-white font-md']")
    # Administration -> Portal Data ->Edit Managers
    ADD_MANAGER_BT = (By.XPATH, "//*[@href='/admin/manager/create']")


class PortalDataNewManagerForm(object):
    # Create Manager Form
    USERNAME_TF = (By.XPATH, "//input[@id='username']")
    PASSWORD_TF = (By.XPATH, "//input[@id='password']")
    SHORTCUT_TF = (By.XPATH, "//input[@name='shortcut']")
    GENDER_DD = (By.XPATH, "")

    ACADEMIC_TILE_TF = (By.XPATH, "//input[@id='academicTitle']")
    FIRST_NAME_TF = (By.XPATH, "//input[@id='firstname']")
    LAST_NAME_TF = (By.XPATH, "//input[@id='lastname']")
    EMAIL_TF = (By.XPATH, "//input[@id='email']")
    EMAIL_SENDER_NAME_TF = (By.XPATH, "//input[@id='emailSender']")
    PHONE_TF = (By.XPATH, "//input[@id='phone']")
    FAX_TF = (By.XPATH, "//input[@id='fax']")
    LANGUAGE_DD = (By.XPATH, "")
    NOTES_TA = (By.XPATH, "//textarea[@id='notes']")
    PORTALS_MS = (By.XPATH, "//select[@id='portals']")
    AUTHORITIES_MS = (By.XPATH, "//select[@id='authorities']")
    SEARCH_CONTEXTS_MS = (By.XPATH, "//select[@id='searchContexts']")
    EMAIL_PICTURE_BT = (By.XPATH, "//input[@name='picture']")
    CANCEL_BT = (By.XPATH, "//*[@href='/admin/manager/list'][@class='btn btn-default']")
    SAVE_BT = (By.XPATH, "//*[@href='javascript:document.editform.submit();']")

    Dict_GenderDropdown = {
        "GENDER_DD_OPTION_MALE": "//select[@id='gender']/option[@value='MALE']",
    }

    Dict_LanguageDropdown = {
        "BG": "//select[@id='language.id']/option[contains(text(),'bg')]",
        "CS": "//select[@id='language.id']/option[contains(text(),'cs')]",
        "DE": "//select[@id='language.id']/option[contains(text(),'de')]",
        "EL": "//select[@id='language.id']/option[contains(text(),'el')]",
        "EN": "//select[@id='language.id']/option[contains(text(),'en')]",
        "ES": "//select[@id='language.id']/option[contains(text(),'es')]",
        "FR": "//select[@id='language.id']/option[contains(text(),'fr')]",
        "HR": "//select[@id='language.id']/option[contains(text(),'hr')]",
        "HU": "//select[@id='language.id']/option[contains(text(),'hu')]",
        "IT": "//select[@id='language.id']/option[contains(text(),'it')]",
        "MK": "//select[@id='language.id']/option[contains(text(),'mk')]",
        "PL": "//select[@id='language.id']/option[contains(text(),'pl')]",
        "PT": "//select[@id='language.id']/option[contains(text(),'pt')]",
        "RO": "//select[@id='language.id']/option[contains(text(),'ro')]",
        "RU": "//select[@id='language.id']/option[contains(text(),'ru')]",
        "SK": "//select[@id='language.id']/option[contains(text(),'sk')]",
        "SL": "//select[@id='language.id']/option[contains(text(),'sl')]",
        "SR": "//select[@id='language.id']/option[contains(text(),'sr')]",
        "UK": "//select[@id='language.id']/option[contains(text(),'uk')]",
        "BS": "//select[@id='language.id']/option[contains(text(),'bs')]",
    }

    Dict_PortalMultiselection = {
        "TS2AT": "//select[@id='portals']/option[@value='TS2AT']",
        "TS2BG": "//select[@id='portals']/option[@value='TS2BG']",
        "TS2CH": "//select[@id='portals']/option[@value='TS2CH']",
        "TS2CO": "//select[@id='portals']/option[@value='TS2CO']",
        "TS2CZ": "//select[@id='portals']/option[@value='TS2CZ']",
        "TS2DE": "//select[@id='portals']/option[@value='TS2DE']",
        "TS2ES": "//select[@id='portals']/option[@value='TS2ES']",
        "TS2GR": "//select[@id='portals']/option[@value='TS2GR']",
        "TS2HR": "//select[@id='portals']/option[@value='TS2HR']",
        "TS2HU": "//select[@id='portals']/option[@value='TS2HU']",
        "TS2LA": "//select[@id='portals']/option[@value='TS2LA']",
        "TS2PL": "//select[@id='portals']/option[@value='TS2PL']",
        "TS2PL2": "//select[@id='portals']/option[@value='TS2PL2']",
        "TS2RO": "//select[@id='portals']/option[@value='TS2RO']",
        "TS2RS": "//select[@id='portals']/option[@value='TS2RS']",
        "TS2SI": "//select[@id='portals']/option[@value='TS2SI']",
        "TS2SK": "//select[@id='portals']/option[@value='TS2SK']",
        "TS2TAI": "//select[@id='portals']/option[@value='TS2TAI']",
        "TS2TS": "//select[@id='portals']/option[@value='TS2TS']",
        "TS2UA": "//select[@id='portals']/option[@value='TS2UA']",
        "TS2UK": "//select[@id='portals']/option[@value='TS2UK']",
        # Authorities
        "ROLE_ADMIN": "//select[@id='authorities']/option[contains(text(),'ROLE_ADMIN')]",
        "ROLE_CCA": "//select[@id='authorities']/option[contains(text(),'ROLE_CCA')]",
        "ROLE_CM": "//select[@id='authorities']/option[contains(text(),'ROLE_CM')]",
        "ROLE_DEU": "//select[@id='authorities']/option[contains(text(),'ROLE_DEU')]",
        "ROLE_DEU_ALL": "//select[@id='authorities']/option[contains(text(),'ROLE_DEU_ALL')]",
        "ROLE_CCA_SP": "//select[@id='authorities']/option[contains(text(),'ROLE_CCA_SP')]",
        "ROLE_DEU_UPLOAD": "//select[@id='authorities']/option[contains(text(),'ROLE_DEU_UPLOAD')]",
        "ROLE_DEU_UPLOAD": "//select[@id='authorities']/option[contains(text(),'ROLE_DEU_UPLOAD')]",
        # SearchContexts
        "BG": "//select[@id='searchContexts']/option[contains(text(),'bg')]",
        "CS": "//select[@id='searchContexts']/option[contains(text(),'cs')]",
        "DE": "//select[@id='searchContexts']/option[contains(text(),'de')]",
        "EL": "//select[@id='searchContexts']/option[contains(text(),'el')]",
        "EN": "//select[@id='searchContexts']/option[contains(text(),'en')]",
        "ES": "//select[@id='searchContexts']/option[contains(text(),'es')]",
        "FR": "//select[@id='searchContexts']/option[contains(text(),'fr')]",
        "HR": "//select[@id='searchContexts']/option[contains(text(),'hr')]",
        "HU": "//select[@id='searchContexts']/option[contains(text(),'hu')]",
        "IT": "//select[@id='searchContexts']/option[contains(text(),'it')]",
        "MK": "//select[@id='searchContexts']/option[contains(text(),'mk')]",
        "PL": "//select[@id='searchContexts']/option[contains(text(),'pl')]",
        "PT": "//select[@id='searchContexts']/option[contains(text(),'pt')]",
        "RO": "//select[@id='searchContexts']/option[contains(text(),'ro')]",
        "RU": "//select[@id='searchContexts']/option[contains(text(),'ru')]",
        "SK": "//select[@id='searchContexts']/option[contains(text(),'sk')]",
        "SL": "//select[@id='searchContexts']/option[contains(text(),'sl')]",
        "SR": "//select[@id='searchContexts']/option[contains(text(),'sr')]",
        "UK": "//select[@id='searchContexts']/option[contains(text(),'uk')]",
        "BS": "//select[@id='searchContexts']/option[contains(text(),'bs')]",

    }
    Dict_AuthoritiesMultiselection = {
        "ROLE_ADMIN": "//select[@id='authorities']/option[contains(text(),'ROLE_ADMIN')]",
        "ROLE_CCA": "//select[@id='authorities']/option[contains(text(),'ROLE_CCA')]",
        "ROLE_CM": "//select[@id='authorities']/option[contains(text(),'ROLE_CM')]",
        "ROLE_DEU": "//select[@id='authorities']/option[contains(text(),'ROLE_DEU')]",
        "ROLE_DEU_ALL": "//select[@id='authorities']/option[contains(text(),'ROLE_DEU_ALL')]",
        "ROLE_CCA_SP": "//select[@id='authorities']/option[contains(text(),'ROLE_CCA_SP')]",
        "ROLE_DEU_UPLOAD": "//select[@id='authorities']/option[contains(text(),'ROLE_DEU_UPLOAD')]",
        "ROLE_DEU_UPLOAD": "//select[@id='authorities']/option[contains(text(),'ROLE_DEU_UPLOAD')]",
    }
    Dict_SearchContextsMultiselection = {
        "BG": "//select[@id='searchContexts']/option[contains(text(),'bg')]",
        "CS": "//select[@id='searchContexts']/option[contains(text(),'cs')]",
        "DE": "//select[@id='searchContexts']/option[contains(text(),'de')]",
        "EL": "//select[@id='searchContexts']/option[contains(text(),'el')]",
        "EN": "//select[@id='searchContexts']/option[contains(text(),'en')]",
        "ES": "//select[@id='searchContexts']/option[contains(text(),'es')]",
        "FR": "//select[@id='searchContexts']/option[contains(text(),'fr')]",
        "HR": "//select[@id='searchContexts']/option[contains(text(),'hr')]",
        "HU": "//select[@id='searchContexts']/option[contains(text(),'hu')]",
        "IT": "//select[@id='searchContexts']/option[contains(text(),'it')]",
        "MK": "//select[@id='searchContexts']/option[contains(text(),'mk')]",
        "PL": "//select[@id='searchContexts']/option[contains(text(),'pl')]",
        "PT": "//select[@id='searchContexts']/option[contains(text(),'pt')]",
        "RO": "//select[@id='searchContexts']/option[contains(text(),'ro')]",
        "RU": "//select[@id='searchContexts']/option[contains(text(),'ru')]",
        "SK": "//select[@id='searchContexts']/option[contains(text(),'sk')]",
        "SL": "//select[@id='searchContexts']/option[contains(text(),'sl')]",
        "SR": "//select[@id='searchContexts']/option[contains(text(),'sr')]",
        "UK": "//select[@id='searchContexts']/option[contains(text(),'uk')]",
        "BS": "//select[@id='searchContexts']/option[contains(text(),'bs')]",
    }

class CMS(object):
    CONFIGURATION_MN = (By.XPATH, "/html[1]/body[1]/aside[1]/nav[1]/ul[1]/li[3]/a[1]/span[1]")
    FLUSH_CACHE_BT = (By.XPATH, "//*[@href='/admin/cmsPage/flushCmsCache']")
    FLUSH_CACHE_ALERT = (By.XPATH, "//div[@class='alert alert-info fade in fade-in-alert']")

class TenderManagement(object):
    TENDER_MANAGEMENT1_BT = (By.XPATH, "//li[2]//a[1]//span[1]")
    # Tender Management1 options
    PUBLISH_TENDER_BT = (By.XPATH, "//a[contains(text(),'Publish Tender')]")
    LIST_OF_ALL_TENDERS_BT = (By.XPATH, "//a[contains(text(),'List of all tenders')]")

    # PUBLISH TENDER OPTIONS
    DEFAULT_TEMPLATE_BT = (By.XPATH, "//*[@href='/admin/tenderViewEdit/createTender/12']")

    ATTACHEMENT_REQUEST_STATS_BT = (By.XPATH, "//li[3]//a[1]//span[1]")
    RESULT_EMAIL_BT = (By.XPATH, "//li[4]//a[1]//span[1]")
    JOBS_AND_PARSERS = (By.XPATH, "//li[5]//a[1]//span[1]")
    TENDER_MANAGEMENT2_BT = (By.XPATH, "//li[6]//a[1]//span[1]")

    class ListOfAllTenders(object):
        MASS_UPDATE_TO_STATUS_DD = (By.XPATH, "//body[@class='desktop-detected smart-style-3']/div[@id='main']/div[@id='content']/div[@id='grid']/div[@id='grid_container']/div[@class='table-responsive']/form[@class='smart-form']/table[@id='dt_basic']/thead/tr/td/div[1]/a[1]")
        MASS_UPDATE_TO_STATUS_INACTIVE = (By.XPATH, "//a[@id='mass-status-update-0']")
        MASS_UPDATE_TO_STATUS_ACTIVE = (By.XPATH, "//a[@id='mass-status-update-1']")
        MASS_UPDATE_TO_STATUS_DELETED = (By.XPATH, "//a[@id='mass-status-update-3']")
        FILTER_BAR = (By.XPATH, "/html[1]/body[1]/div[3]/div[2]/form[3]/fieldset[1]/div[1]/div[2]/div[1]/div[1]")
        MANAGER_EDITED_DD = (By.XPATH, "/select[@id='managerEditedId']")
        SEARCH_BT = (By.XPATH, "//input[@id='submitButton']")
        ALL_RECORDS_CB = (By.XPATH, "//table[@id='dt_basic']//thead//tr//th//label[@class='checkbox']//i")
        ALL_TITLES = (By.XPATH, "//*[contains(@class,'tenderLink linkNotDecorated')]")
        ALL_STATUS = (By.XPATH, "/html[1]/body[1]/div[3]/div[2]/div[2]/div[1]/div[1]/form[1]/table[1]/tbody[1]//td[3]")
        ALL_PUBLICAION_DATE = (By.XPATH, "/html[1]/body[1]/div[3]/div[2]/div[2]/div[1]/div[1]/form[1]/table[1]/tbody[1]//td[7]")
        ALL_TYPES_OF_DOCUMENTS = (By.XPATH, "/html[1]/body[1]/div[3]/div[2]/div[2]/div[1]/div[1]/form[1]/table[1]/tbody[1]//td[9]")
        ALL_NATURE_OF_CONTRACT = (By.XPATH, "/html[1]/body[1]/div[3]/div[2]/div[2]/div[1]/div[1]/form[1]/table[1]/tbody[1]//td[10]")
        ALL_NUTS = (By.XPATH, "/html[1]/body[1]/div[3]/div[2]/div[2]/div[1]/div[1]/form[1]/table[1]/tbody[1]//td[12]")

        ROW_1_TILTE = (By.XPATH, "//div[@id='main']//tr[1]//td[6]")
        ROW_1_EDIT = (By.XPATH, "//div[@id='main']//tbody//tr[1]//*[@class='fa fa-lg fa-pencil']")
        ROw_2_TILTE = (By.XPATH, "//div[@id='main']//tr[2]//td[6]")
        ROW_2_EDIT = (By.XPATH, "//div[@id='main']//tbody//tr[2]//*[@class='fa fa-lg fa-pencil']")

        Dict_MANAGER_EDITED_DD = {
            "pako": "//select[@id='managerEditedId']/option[contains(text(),'pako')]",
        }


class TenderTemplate(object):
    # Actions buttons
    SAVE_AND_BACK_BT = (By.XPATH, "/html[1]/body[1]/div[3]/div[2]/div[1]/div[1]/div[1]/a[5]")


    # Required fields
    TITLE_TA = (By.XPATH, "//textarea[@name='title_en']")
    LANGUAGE_DD = (By.XPATH, "//select[@id='currentTenderLang']")
    FULL_TEXT_RICH_EDITOR = (By.CLASS_NAME, "cke_wysiwyg_frame")
    PUBLICATION_DATE_TF = (By.XPATH, "//input[@id='publicationDateInput']")
    SYSTEM_EXPIRATION_DATE_TF = (By.XPATH, "//input[@id='systemExpirationDateInput']")
    NATURE_OF_CONTRACT_DD = (By.XPATH, "//select[@id='natureOfContract']")
    TYPE_OF_DOCUMENT_DD = (By.XPATH, "//select[@id='typeOfDocument']")
    STATUS_DD = (By.XPATH, "//select[@id='status']")
    NUTS_CODE_REALISATION_ADDRESS_TF = (By.XPATH, "//input[@id='0_realisationAddressNutsCode']")

    Dict_STATUS_DD = {
        "PLEASE_SELECT": "//select[@id='status']/option[@value='Please Select']",
        "NEW": "//select[@id='status']/option[@value='NEW']",
        "INACTIVE": "//select[@id='status']/option[@value='INACTIVE']",
        "ACTIVE": "//select[@id='status']/option[@value='ACTIVE']",
        "EXPIRED": "//select[@id='status']/option[@value='EXPIRED']",
        "DELETED": "//select[@id='status']/option[@value='DELETED']",
    }

    Dict_NATURE_OF_CONTRACT_DD = {
        "PLEASE_SELECT": "//select[@id='natureOfContract']/option[@value='Please Select']",
        "PUBLIC_WORKS_CONTRACT": "//select[@id='natureOfContract']/option[@value='PUBLIC_WORKS_CONTRACT']",
        "SUPPLY_CONTRACT": "//select[@id='natureOfContract']/option[@value='SUPPLY_CONTRACT']",
        "COMBINED_CONTRACT": "//select[@id='natureOfContract']/option[@value='COMBINED_CONTRACT']",
        "SERVICE_CONTRACT": "//select[@id='natureOfContract']/option[@value='SERVICE_CONTRACT']",
        "OTHER": "//select[@id='natureOfContract']/option[@value='OTHER']",
    }

    Dict_TYPE_OF_DOCUMENT_DD = {
        "PLEASE_SELECT": "//select[@id='typeOfDocument']/option[@value='Please Select']",
        "TENDER": "//select[@id='typeOfDocument']/option[@value='TENDER']",
        "CORRECTION": "//select[@id='typeOfDocument']/option[@value='CORRECTION']",
        "BUDGET": "//select[@id='typeOfDocument']/option[@value='BUDGET']",
        "PROCUREMENT_PLAN": "//select[@id='typeOfDocument']/option[@value='PROCUREMENT_PLAN']",
        "PRIOR_INFORMATION": "//select[@id='typeOfDocument']/option[@value='PRIOR_INFORMATION']",
        "CONSULTATION": "//select[@id='typeOfDocument']/option[@value='CONSULTATION']",
        "DECISION": "//select[@id='typeOfDocument']/option[@value='DECISION']",
        "RESULT": "//select[@id='typeOfDocument']/option[@value='RESULT']",
        "CONTRACT": "//select[@id='typeOfDocument']/option[@value='CONTRACT']",
        "PAYMENT_ORDER": "//select[@id='typeOfDocument']/option[@value='PAYMENT_ORDER']",
        "OTHER_INFORMATION": "//select[@id='typeOfDocument']/option[@value='OTHER_INFORMATION']",
    }

    Dict_LANGUAGE_DD = {
        "BG": "//select[@id='currentTenderLang']/option[@value='bg']",
        "CS": "//select[@id='currentTenderLang']/option[@value='cs']",
        "DE": "//select[@id='currentTenderLang']/option[@value='de']",
        "EL": "//select[@id='currentTenderLang']/option[@value='el']",
        "EN": "//select[@id='currentTenderLang']/option[@value='en']",
        "ES": "//select[@id='currentTenderLang']/option[@value='es']",
        "FR": "//select[@id='currentTenderLang']/option[@value='fr']",
        "HR": "//select[@id='currentTenderLang']/option[@value='hr']",
        "HU": "//select[@id='currentTenderLang']/option[@value='hu']",
        "IT": "//select[@id='currentTenderLang']/option[@value='it']",
        "MK": "//select[@id='currentTenderLang']/option[@value='mk']",
        "PL": "//select[@id='currentTenderLang']/option[@value='pl']",
        "PT": "//select[@id='currentTenderLang']/option[@value='pt']",
        "RO": "//select[@id='currentTenderLang']/option[@value='ro']",
        "RU": "//select[@id='currentTenderLang']/option[@value='ru']",
        "SK": "//select[@id='currentTenderLang']/option[@value='sk']",
        "SL": "//select[@id='currentTenderLang']/option[@value='sl']",
        "SR": "//select[@id='currentTenderLang']/option[@value='sr']",
        "UK": "//select[@id='currentTenderLang']/option[@value='uk']",
        "BS": "//select[@id='currentTenderLang']/option[@value='bs']",
    }

class PortalPageUK(object):
    LOGIN_LB = (By.XPATH, "/html[1]/body[1]/div[8]/div[3]/div[1]/div[1]/a[1]")
    LOGIN_TF = (By.XPATH, "//input[@id='login-username']")
    PASSWORD_BEFORE_CLICK_TF = (By.XPATH, "//input[@id='fake-login-pass']")
    PASSWORD_AFTER_CLICK_TF = (By.XPATH, "//input[@id='login-pass']")
    LOGIN_BT = (By.XPATH, "//input[@id='submit_login']")


    #Main Menu
    PRODUCTS_LB = (By.XPATH, "//*[@href='/wizardRegistration/order']")
    RECOMMENDED_PRODUCT = (By.XPATH, "//div[@class='recommendedProductBody']")

    #Logged
    USER_ID_LB = (By.XPATH, "//li[@class='item-profile user-id current']")

