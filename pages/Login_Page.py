from pages.Base_Page import BasePage
from locators import *


class LoginPage(BasePage):

    def fill_credentials(self, email, password):
        self.driver.find_element(*LoginPageLocators.USERNAME_TF).send_keys(email)
        self.driver.find_element(*LoginPageLocators.PASSWORD_TF).send_keys(password)
        self.driver.find_element(*LoginPageLocators.SUBMIT_BT).click()
