from pages.Base_Page import BasePage
from locators import *
import pytest


class TenderAdminPage(BasePage):

    def get_whole_text_from_titles(self, types, text_from, count):
        elements = self.driver.find_elements(types, text_from)
        texts = []
        for x in range(0, count):
            titles = elements[x].text
            texts.append(titles)
        return texts

    def verify_tenders_from_global_variables(self):
        count = self.count_rows_from_tenders_table()
        print('------------')
        all_titless = self.get_whole_text_from_titles(*TenderManagement.ListOfAllTenders.ALL_TITLES, count)
        all_nature_of_contaract = self.get_whole_text_from_titles(
            *TenderManagement.ListOfAllTenders.ALL_NATURE_OF_CONTRACT, count)
        all_status = self.get_whole_text_from_titles(*TenderManagement.ListOfAllTenders.ALL_STATUS, count)
        all_publication_date = self.get_whole_text_from_titles(*TenderManagement.ListOfAllTenders.ALL_PUBLICAION_DATE,
                                                               count)
        all_types_of_document = self.get_whole_text_from_titles(
            *TenderManagement.ListOfAllTenders.ALL_TYPES_OF_DOCUMENTS, count)
        all_nutes = self.get_whole_text_from_titles(*TenderManagement.ListOfAllTenders.ALL_NUTS, count)

        self.compare_lists(all_titless, pytest.title_tab)
        self.compare_lists(all_nature_of_contaract, pytest.nature_of_contract_tab)
        self.compare_lists(all_status, pytest.status_tab)
        self.compare_lists(all_publication_date, pytest.publication_date_tab)
        self.compare_lists(all_types_of_document, pytest.type_of_document_tab)
        self.compare_lists(all_nutes, pytest.nuts_tab)
