from locators import *
import time
import pytest
import datetime
import os
from datetime import date, timedelta
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import StaleElementReferenceException, TimeoutException
import selenium.webdriver.support.ui as ui

from selenium.webdriver.common.keys import Keys


class BasePage():
    """Base class to initialize the base page that will be called from all pages"""

    #
    def __init__(self, driver):

        self.driver = driver

    def wait_for_x_sec(self, amount):
        time.sleep(amount)

    def fill_field(self, types, field, text):
        element = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((types, field))
        )
        element.clear()
        element.send_keys(text)

    def fill_date(self, types, field, text):
        element = self.driver.find_element(types, field)
        self.driver.execute_script("arguments[0].value=arguments[1]", element, text)

    def find_text_on_the_page(self, list_of_text, text):
        for i in list_of_text:
            if text.find(i) != -1:
                assert True, 'True' + i
                print("true", i)
            else:
                assert False, 'False' + i

    def ensure_element_is_displayed(self, types, element):
        if WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((types, element))
        ):
            print('Element with XPATH: ', element, ' is displayed.')
        else:
            print('Element with XPATH: ', element, ' is not displayed.')

    def ensure_elements_are_displayed(self, types, element):
        if WebDriverWait(self.driver, 10).until(
                EC.presence_of_all_elements_located((types, element))
        ):
            print('Elements with XPATH: ', element, ' are displayed.')
        else:
            print('Elements with XPATH: ', element, 'is not displayed.')

    def select_checkbox(self, types, element):
        element = self.driver.find_element(types, element)
        actionchains = ActionChains(self.driver)
        actionchains.click(element).perform()

    def alert_handling(self):
        try:
            WebDriverWait(self.driver, 3).until(EC.alert_is_present(),
                                                'Timed out waiting for PA creation ' +
                                                'confirmation popup to appear.')

            alert = self.driver.switch_to.alert
            alert.accept()
            print("Alert accepted")
            alert = self.driver.switch_to.default_content()
        except TimeoutException:
            print("No alert")

    def click_element(self, types, element):
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((types, element)) and EC.presence_of_element_located((types, element))
            )
            element.click()
        except StaleElementReferenceException as e:
            self.wait_for_x_sec(.300)
            self.driver.find_element(types, element).click()

    def select_dropdown_option_data_driven(self, element):
        self.driver.find_element_by_xpath(element).click()

    def select_multiple_options_data_driven(self, element_datadriven):
        elementpath = element_datadriven.split('$')
        for x in range(0, len(elementpath)):
            try:
                element = WebDriverWait(self.driver, 10).until(
                    EC.presence_of_element_located(
                        (By.XPATH, PortalDataNewManagerForm.Dict_PortalMultiselection[elementpath[x]]))
                )
                element.click()
            except StaleElementReferenceException as e:
                self.wait_for_x_sec(.300)
                self.driver.find_element(By.XPATH, element).click()
            print(elementpath[x])

    def upload_file(self, element):
        self.driver.find_element(*PortalDataNewManagerForm.EMAIL_PICTURE_BT).send_keys(os.getcwd() + element)

    def ensure_titles_are_displayed(self, title):
        self.wait_for_x_sec(1)
        print("Ttile from test: ", title, 'Title from page: ', self.driver.title)
        wait_for_title = WebDriverWait(self.driver, 10).until(lambda x: title in self.driver.title)

    def get_whole_text_from_elements(self, types, text_from):
        element = self.driver.find_elements(types, text_from)
        whole_text = element[0].text
        print(element[0].text)
        return whole_text

    def comparison_texts(self, element_from_page, element):
        if (str(element_from_page) in str(element)):
            print("True " + str(element_from_page) + " is in " + str(element))
            assert True, 'True'
        else:
            print("False " + str(element_from_page) + " is not in " + str(element))
            assert False, 'False'

    def comparison_texts_notin(self, element_from_page, element):
        if (str(element_from_page) not in str(element)):
            print("True " + str(element_from_page) + " is not in " + str(element))
            assert True, 'True'
        else:
            print("False " + str(element_from_page) + " is in " + str(element))
            assert False, 'False'

    def compare_lists(self, a, b):
        if set(a) == set(b):
            assert True, 'True'
            print("true", a, 'contains', b)
        else:
            print("true", a, 'not contains', b)
            assert False, 'False'

    def count_rows_from_tenders_table(self):
        count = len(self.driver.find_elements(*TenderManagement.ListOfAllTenders.ALL_TITLES))
        return count

    def get_whole_texts_from_DD_dropdowns(self, xpath):
        elements = self.driver.find_element_by_xpath(xpath)
        whole_text = elements.text
        print(whole_text)
        return whole_text
