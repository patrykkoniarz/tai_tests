from pages.Base_Page import BasePage
from locators import *
import pytest


class TenderForm(BasePage):

    def fill_field_rich_text_editor(self, text):
        ckeditor_frame = self.driver.find_element(By.XPATH,
                                                  "//iframe[@class='cke_wysiwyg_frame cke_reset'][@aria-describedby='cke_372']")
        self.driver.switch_to.frame(ckeditor_frame)
        ck_editor_body = self.driver.find_element(By.TAG_NAME, 'body')
        ck_editor_body.send_keys(text)
        self.driver.switch_to.default_content()

    def save_tenders_fields_to_global_variables(self, title, publication_date, nature_of_contract, type_of_document,
                                                status, realisation_address_nuts):
        pytest.title_tab.append(title)
        pytest.publication_date_tab.append(publication_date)
        pytest.nuts_tab.append(realisation_address_nuts)
        pytest.nature_of_contract_tab.append(
            self.get_whole_texts_from_DD_dropdowns(TenderTemplate.Dict_NATURE_OF_CONTRACT_DD[nature_of_contract]))
        pytest.type_of_document_tab.append(
            self.get_whole_texts_from_DD_dropdowns(TenderTemplate.Dict_TYPE_OF_DOCUMENT_DD[type_of_document]))
        pytest.status_tab.append(self.get_whole_texts_from_DD_dropdowns(TenderTemplate.Dict_STATUS_DD[status]))
