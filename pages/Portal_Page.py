import pytest
from pages.Base_Page import BasePage
from locators import *


class PortalPage(BasePage):

    def login_as_customer(self, email, password):

        self.driver.find_element(*PortalPageUK.LOGIN_LB).click()
        self.driver.find_element(*PortalPageUK.LOGIN_TF).send_keys(email)
        self.driver.find_element(*PortalPageUK.PASSWORD_BEFORE_CLICK_TF).click()
        self.driver.find_element(*PortalPageUK.PASSWORD_AFTER_CLICK_TF).send_keys(password)
        self.driver.find_element(*PortalPageUK.LOGIN_BT).click()
        self.ensure_element_is_displayed(*PortalPageUK.USER_ID_LB)

    def portal_checking(self):
        main_window = self.driver.current_window_handle
        self.click_element(*TenderAdmin.PORTAL_BT)
        handles = self.driver.window_handles
        size = len(handles)
        for x in range(size):
            if handles[x] != self.driver.current_window_handle:
                self.driver.switch_to.window(handles[x])
                print(self.driver.title)
                self.ensure_titles_are_displayed(
                    "Public tenders from the UK, Ireland and Europe - tender-service.co.uk")
                self.ensure_element_is_displayed(By.XPATH, "//*[@id='logoImg']")
                return main_window

    def portal_checking_end(self, main_window):

        self.driver.close()
        self.driver.switch_to.window(main_window)
