import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


def pytest_configure():
    # Credentials
    pytest.credentials_admin = ('pako', 'PXaRtZuH!4')
    pytest.credentials_manager = ('Tester', 'Tester123')
    pytest.credentials_customer = ('test_automation@mail.com', 'testing1')

    # Tender result list tabs
    pytest.title_tab = []
    pytest.publication_date_tab = []
    pytest.type_of_document_tab = []
    pytest.nature_of_contract_tab = []
    pytest.nuts_tab = []
    pytest.status_tab = []


@pytest.fixture(scope="class")
def setup(request):
    print("initiating driver")
    options = Options()
    # options.add_argument("--headless")
    options.add_argument('--no-sandbox')
    options.add_argument("--disable-setuid-sandbox")
    options.add_argument('--ignore-certificate-errors')
    options.add_argument("--start-maximized")
    options.add_argument("--window-size=1920,1080")
    # driver = webdriver.Remote("http://127.0.0.1:4444/wd/hub", options=options)
    # driver = webdriver.Chrome(executable_path="C:\\Selenium_Drivers\\chromedriver.exe", options=options)
    driver = webdriver.Chrome(
        executable_path="/home/pakoni/Projects/TIA_automation/Selenium_drivers/chromedriver_linux64/chromedriver",
        options=options)

    driver.get("https://test:ariadne@test.tender-service.co.uk/admin")
    driver.implicitly_wait(5)

    request.cls.driver = driver
    yield driver
    driver.close()
