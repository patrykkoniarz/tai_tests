import pytest
from locators import *
from data_driven import *
from pages.Portal_Page import PortalPage
from pages.Login_Page import LoginPage
from pages.Tender_Admin_Page import TenderAdminPage
from pages.Tender_Form_Page import TenderForm


@pytest.mark.usefixtures("setup")
class TestBrowser():

    def test_Page_Load(self):
        assert "tender-service.co.uk: Login" in self.driver.title

    # Admin: Login with incorect login and password /// This TC verify if user cannot login as a admin with incorect login and password
    def test_Sign_In_Negative(self):
        driver = LoginPage(self.driver)
        driver.fill_credentials('pako', 'wrong_password')
        driver.ensure_element_is_displayed(*LoginPageLocators.ALERT_TM)
        driver.ensure_titles_are_displayed("tender-service.co.uk: Login")

    # Admin: Logout as admin /// This TC verify if admin can loggout from system.
    def test_Sign_In_And_Out_Positive(self):
        driver = LoginPage(self.driver)
        driver.fill_credentials(*pytest.credentials_admin)
        driver.ensure_titles_are_displayed("tender-service.co.uk: Tender Admin")
        # driver.wait_for_x_sec(2)
        driver.ensure_elements_are_displayed(*TenderAdmin.ALL_MENU_OPTIONS)
        driver.click_element(*TenderAdmin.LOGOUT_BT)
        driver.ensure_titles_are_displayed("tender-service.co.uk: Login")

    # Admin: Login as admin /// This TC verify if user can login as a admin into system
    def test_Sign_In_Positive(self):
        driver = LoginPage(self.driver)
        driver.fill_credentials(*pytest.credentials_admin)
        driver.ensure_titles_are_displayed("tender-service.co.uk: Tender Admin")
        driver.ensure_elements_are_displayed(*TenderAdmin.ALL_MENU_OPTIONS)

    # Admin: Create Manager with access to the all portals /// This TC verify if admin is able to create manager with access to the all portals.
    @pytest.mark.parametrize(
        new_manager_parameters,
        new_manager_DataDriven)
    def test_Create_Managers(self, username, password, shortcut, academictitle, gender, firstname, lastname, email,
                             emailsender, phone, fax, language, notes, portal, authorities, searchcontexts,
                             emailpicture):
        driver = TenderAdminPage(self.driver)
        driver.ensure_elements_are_displayed(*TenderAdmin.ALL_MENU_OPTIONS)
        driver.click_element(*TenderAdmin.ADMINISTRATION_LB)
        work_view_title = driver.get_whole_text_from_elements(*PortalData.VIEW_TITLE_BAR)
        driver.comparison_texts(work_view_title, "Manager list")
        driver.click_element(*TenderAdmin.EDIT_MANAGERS_LB)
        driver.click_element(*PortalData.ADD_MANAGER_BT)
        driver.fill_field(*PortalDataNewManagerForm.USERNAME_TF, username)
        driver.fill_field(*PortalDataNewManagerForm.PASSWORD_TF, password)
        driver.fill_field(*PortalDataNewManagerForm.SHORTCUT_TF, shortcut)
        driver.select_dropdown_option_data_driven(PortalDataNewManagerForm.Dict_GenderDropdown[gender])
        driver.fill_field(*PortalDataNewManagerForm.ACADEMIC_TILE_TF, academictitle)
        driver.fill_field(*PortalDataNewManagerForm.FIRST_NAME_TF, firstname)
        driver.fill_field(*PortalDataNewManagerForm.LAST_NAME_TF, lastname)
        driver.fill_field(*PortalDataNewManagerForm.EMAIL_TF, email)
        driver.fill_field(*PortalDataNewManagerForm.EMAIL_SENDER_NAME_TF, emailsender)
        driver.fill_field(*PortalDataNewManagerForm.PHONE_TF, phone)
        driver.fill_field(*PortalDataNewManagerForm.FAX_TF, fax)
        driver.select_dropdown_option_data_driven(PortalDataNewManagerForm.Dict_LanguageDropdown[language])
        driver.fill_field(*PortalDataNewManagerForm.NOTES_TA, notes)
        driver.select_multiple_options_data_driven(portal)
        driver.select_multiple_options_data_driven(authorities)
        driver.select_multiple_options_data_driven(searchcontexts)
        driver.upload_file(emailpicture)

    # Admin: Create new tender /// This TC verify if Admin can create new tender
    @pytest.mark.parametrize(
        new_tender_parameters,
        new_tender_DataDriven)
    def test_Create_Tenders(self, fulltext, language, title, publication_date, system_expiration_date,
                            nature_of_contract, type_of_document, status, realisation_address_nuts):
        driver = TenderForm(self.driver)
        driver.click_element(*TenderAdmin.TENDER_MANAGEMENT_LB)
        driver.click_element(*TenderManagement.TENDER_MANAGEMENT1_BT)
        driver.click_element(*TenderManagement.PUBLISH_TENDER_BT)
        driver.click_element(*TenderManagement.DEFAULT_TEMPLATE_BT)
        driver.fill_field_rich_text_editor(fulltext)
        driver.fill_field(*TenderTemplate.TITLE_TA, title)
        driver.select_dropdown_option_data_driven(TenderTemplate.Dict_LANGUAGE_DD[language])
        driver.select_dropdown_option_data_driven(TenderTemplate.Dict_NATURE_OF_CONTRACT_DD[nature_of_contract])
        driver.fill_date(*TenderTemplate.PUBLICATION_DATE_TF, publication_date)
        driver.fill_date(*TenderTemplate.SYSTEM_EXPIRATION_DATE_TF, system_expiration_date)
        driver.select_dropdown_option_data_driven(TenderTemplate.Dict_TYPE_OF_DOCUMENT_DD[type_of_document])
        driver.select_dropdown_option_data_driven(TenderTemplate.Dict_STATUS_DD[status])
        driver.fill_field(*TenderTemplate.NUTS_CODE_REALISATION_ADDRESS_TF, realisation_address_nuts)
        driver.save_tenders_fields_to_global_variables(title, publication_date, nature_of_contract, type_of_document,
                                                       status, realisation_address_nuts)

    # Admin: Tenders Verfication // This TC verify if all tenders created in previous test case exist.
    # Admin: Edit Tender // This TC verify if User is able to edit and save tender.
    def test_Verify_Tenders(self):
        driver = TenderAdminPage(self.driver)
        driver.click_element(*TenderAdmin.TENDER_MANAGEMENT_LB)
        driver.click_element(*TenderManagement.TENDER_MANAGEMENT1_BT)
        driver.click_element(*TenderManagement.LIST_OF_ALL_TENDERS_BT)
        driver.click_element(*TenderManagement.ListOfAllTenders.FILTER_BAR)
        driver.select_dropdown_option_data_driven(TenderManagement.ListOfAllTenders.Dict_MANAGER_EDITED_DD["pako"])
        driver.click_element(*TenderManagement.ListOfAllTenders.SEARCH_BT)
        driver.wait_for_x_sec(2)
        driver.verify_tenders_from_global_variables()

    def test_Mass_Update(self):
        driver = TenderAdminPage(self.driver)
        driver.click_element(*TenderAdmin.TENDER_MANAGEMENT_LB)
        driver.click_element(*TenderManagement.TENDER_MANAGEMENT1_BT)
        driver.click_element(*TenderManagement.LIST_OF_ALL_TENDERS_BT)
        driver.click_element(*TenderManagement.ListOfAllTenders.FILTER_BAR)
        driver.select_dropdown_option_data_driven(TenderManagement.ListOfAllTenders.Dict_MANAGER_EDITED_DD["pako"])
        driver.click_element(*TenderManagement.ListOfAllTenders.SEARCH_BT)
        driver.wait_for_x_sec(2)
        count = driver.count_rows_from_tenders_table()
        all_status = driver.get_whole_text_from_titles(*TenderManagement.ListOfAllTenders.ALL_STATUS, count)
        driver.click_element(*TenderManagement.ListOfAllTenders.ALL_RECORDS_CB)
        driver.select_checkbox(*TenderManagement.ListOfAllTenders.MASS_UPDATE_TO_STATUS_DD)
        driver.select_checkbox(*TenderManagement.ListOfAllTenders.MASS_UPDATE_TO_STATUS_INACTIVE)
        driver.alert_handling()
        driver.wait_for_x_sec(2)
        driver.click_element(*TenderManagement.ListOfAllTenders.SEARCH_BT)
        all_status2 = driver.get_whole_text_from_titles(*TenderManagement.ListOfAllTenders.ALL_STATUS, count)
        driver.comparison_texts_notin(all_status, all_status2)

    def test_PortalCheck(self):
        driver = PortalPage(self.driver)
        mainwindow = driver.portal_checking()
        driver.login_as_customer(*pytest.credentials_customer)
        # driver.click_element(*PortalPageUK.PRODUCTS_LB)
        # driver.ensure_element_is_displayed(*PortalPageUK.RECOMMENDED_PRODUCT)
        driver.portal_checking_end(mainwindow)

    # Login as manager /// This TC verify user can loggin into system as a Manager
    # Login as manager and "flush the CMS Cache" (in the CMS section)
    # Logout  as manager /// This TC verify if Manager can loggout from the system.

    def test_Login_As_Manager(self):
        driver = LoginPage(self.driver)
        # Login as manager
        driver.ensure_elements_are_displayed(*TenderAdmin.ALL_MENU_OPTIONS)
        driver.click_element(*TenderAdmin.LOGOUT_BT)
        # driver.wait_for_x_sec(2)
        driver.ensure_titles_are_displayed("tender-service.co.uk: Login")
        driver.fill_credentials(*pytest.credentials_manager)
        driver.ensure_titles_are_displayed("tender-service.co.uk: Tender Admin")
        driver.click_element(*TenderAdmin.ADMINISTRATION_LB)
        user_name = driver.get_whole_text_from_elements(*TenderAdmin.USER_NAME_LB)
        driver.comparison_texts(user_name, "Tester")
        # Login as manager and "flush the CMS Cache" (in the CMS section)
        driver.click_element(*TenderAdmin.CMS_LB)
        driver.click_element(*CMS.CONFIGURATION_MN)
        driver.wait_for_x_sec(1)
        driver.click_element(*CMS.FLUSH_CACHE_BT)
        driver.ensure_element_is_displayed(*CMS.FLUSH_CACHE_ALERT)
        driver.comparison_texts(driver.get_whole_text_from_elements(*CMS.FLUSH_CACHE_ALERT),
                                'CMS cache flushed successfully!')
        # Logout  as manager
        driver.click_element(*TenderAdmin.LOGOUT_BT)
        driver.ensure_titles_are_displayed("tender-service.co.uk: Login")

    # def test_DB_Cleaning(self):
    #     cnx = mysql.connector.connect(
    #         host="127.0.0.1",
    #         port=3306,
    #         user="root",
    #         password="admin")
    #
    #     # Get a cursor
    #     cur = cnx.cursor()
    #
    #     # Execute a query
    #     cur.execute("")
    #
    #     # Fetch one result
    #     row = cur.fetchone()
    #     #print("Current date is: {0}".format(row[0]))
    #
    #     # Close connection
    #     cnx.close()
