import pytest
from locators import *
from pages.Login_Page import LoginPage


@pytest.mark.usefixtures("setup")
class TestLogin():

    def test_Page_Load(self):
        assert "tender-service.co.uk: Login" in self.driver.title

    # Admin: Login with incorect login and password /// This TC verify if user cannot login as a admin with incorect login and password
    def test_Sign_In_Negative(self):
        driver = LoginPage(self.driver)
        driver.fill_credentials('pako', 'wrong_password')
        driver.ensure_element_is_displayed(*LoginPageLocators.ALERT_TM)
        driver.ensure_titles_are_displayed("tender-service.co.uk: Login")

    # Admin: Logout as admin /// This TC verify if admin can loggout from system.
    def test_Sign_In_And_Out_Positive(self):
        driver = LoginPage(self.driver)
        driver.fill_credentials(pytest.credentials_admin)
        driver.ensure_titles_are_displayed("tender-service.co.uk: Tender Admin")
        driver.wait_for_x_sec(2)
        driver.ensure_elements_are_displayed(*TenderAdmin.ALL_MENU_OPTIONS)
        driver.click_element(*TenderAdmin.LOGOUT_BT)
        driver.ensure_titles_are_displayed("tender-service.co.uk: Login")

    # Admin: Login as admin /// This TC verify if user can login as a admin into system
    def test_Sign_In_Positive(self):
        driver = LoginPage(self.driver)
        driver.fill_credentials(pytest.credentials_admin)
        driver.ensure_titles_are_displayed("tender-service.co.uk: Tender Admin")
        driver.ensure_elements_are_displayed(*TenderAdmin.ALL_MENU_OPTIONS)

    def test_Login_As_Manager(self):
        driver = LoginPage(self.driver)
        # Login as manager
        driver.ensure_elements_are_displayed(*TenderAdmin.ALL_MENU_OPTIONS)
        driver.click_element(*TenderAdmin.LOGOUT_BT)
        driver.wait_for_x_sec(2)
        driver.ensure_titles_are_displayed("tender-service.co.uk: Login")
        driver.fill_credentials(pytest.credentials_manager)
        driver.ensure_titles_are_displayed("tender-service.co.uk: Tender Admin")
        driver.click_element(*TenderAdmin.ADMINISTRATION_LB)
        user_name = driver.get_whole_text_from_elements(*TenderAdmin.USER_NAME_LB)
        driver.comparison_texts(user_name, "Tester")
        # Login as manager and "flush the CMS Cache" (in the CMS section)
        driver.click_element(*TenderAdmin.CMS_LB)
        driver.click_element(*CMS.CONFIGURATION_MN)
        driver.wait_for_x_sec(1)
        driver.click_element(*CMS.FLUSH_CACHE_BT)
        driver.ensure_element_is_displayed(*CMS.FLUSH_CACHE_ALERT)
        driver.comparison_texts(driver.get_whole_text_from_elements(*CMS.FLUSH_CACHE_ALERT),
                                'CMS cache flushed successfully!')
        # Logout  as manager
        driver.click_element(*TenderAdmin.LOGOUT_BT)
        driver.ensure_titles_are_displayed("tender-service.co.uk: Login")
