new_manager_parameters_list = ['username', 'password', 'shortcut', 'academictitle', 'gender', 'firstname', 'lastname', 'email', 'emailsender', 'phone', 'fax', 'language', 'notes', 'portal', 'authorities', 'searchcontexts', 'emailpicture']
new_manager_parameters = 'username, password, shortcut, academictitle, gender, firstname, lastname, email, emailsender, phone, fax, language, notes, portal, authorities, searchcontexts, emailpicture'


new_manager_DataDriven = []
with open('datasets/PortalDataNewManagers.txt', 'r') as f:
    for line in (f.readlines()[1:]):
        new_manager_parameters_list = line.strip().split(
            ',')
        new_manager_DataDriven.append(
            new_manager_parameters_list)
print(new_manager_DataDriven)


new_tender_parameters_list = ['fulltext', 'language', 'title', 'publication_date', 'system_expiration_date', 'nature_of_contract', 'type_of_document', 'status', 'realisation_address_nuts']
new_tender_parameters = 'fulltext, language, title, publication_date, system_expiration_date, nature_of_contract, type_of_document, status, realisation_address_nuts'


new_tender_DataDriven = []
with open('datasets/DefaultTenderDataSet.txt', 'r') as f:
    for line in (f.readlines()[1:]):
        new_tender_parameters_list = line.strip().split(
            ',')
        new_tender_DataDriven.append(
            new_tender_parameters_list)
print(new_tender_DataDriven)

